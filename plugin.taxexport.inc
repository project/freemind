<?php

/**
 * Implmentation of freemind_plugin_menu()
 *
 * @return array array of menu items
 */
function taxexport_freemind_plugin_menu(){
  $items = array();

  $items['admin/content/taxonomy/%/freemind'] = array(
    'title' => 'Freemind View',
    'page callback' => '_freemind_display_applet',
    'access arguments' => array('access content'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/content/taxonomy/%/mm'] = array(
    'title' => 'Export MM file',
    'page callback' => '_freemind_display_mm',
    'access arguments' => array('access content'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Controls the mm rendering process
 *
 * @param integer $vid vocabulary id
 * @return string the raw mm file
 */
function _freemind_taxexport_render_mm($vid){
  $tree = taxonomy_get_tree($vid);
  $v = taxonomy_vocabulary_load($vid);

  // validating the tree; we can't handle multiple parents right now
  if(!_freemind_validate_tree($tree)){
    drupal_set_message(t(''), 'error');
    return FALSE;
  }

  $root_term = new stdClass;
  $root_term->tid = 0;
  $root_term->name = $v->name;
  $root_term->parents = array();

  array_unshift($tree, $root_term);

  $mm = "<map version=\"0.9.0\">\n";
  $mm .= _freemind_taxexport_render($tree);
  $mm .= '</map>';

  return $mm;
}

/**
 * Recursive function to traverse the taxonomy tree
 *
 * @param array $arr array with term objects
 * @param integer $item the id of the current item, do not touch it
 * @return string rendered freemind nodes
 */
function _freemind_taxexport_render($arr, $item=0){
  # TODO: speed improvement! now: O(n^2)
  /**
   * the <node> requires the following attributes:
   * - CREATED
   *  timestamp
   * - ID
   *  custom unique ID, should start with ID_
   * - MODIFIED
   *  timestamp
   * - TEXT
   *  content
   * optional:
   * - POSITION
   *  could be left or right; only set on the first level
   */
  $ret  = '<node CREATED="'.time().'" ID="ID_'.$arr[$item]->tid.'" MODIFIED="'.time().'" TEXT="'.$arr[$item]->name."\">\n";
  # attributes
  if($item > 0)
    foreach(array_merge_recursive(
        array(
          'description'=>$arr[$item]->description
        ),
        _freemind_plugin_invoke_all('freemind_custom_attribute', array($arr[$item])))
      as $k=>$v)
      $ret .= '<attribute NAME="'.$k.'" VALUE="'.$v."\" />\n";
  # children
  foreach($arr as $k=>$v)
    if(in_array($arr[$item]->tid, $v->parents))
      $ret .= _freemind_taxexport_render($arr, $k);
  $ret .= "</node>\n";
  return $ret;
}

/**
 * Validates the terms. Right now, it fails when a term has multiple parents.
 *
 * @todo think twice about multiple parents, and if it possible remove this restriction
 * @param array $tree array of term objects
 * @return boolean validation result
 */
function _freemind_validate_tree($tree){
  foreach($tree as $t)
    if(count($t->parents) > 1)
      return false;
  return true;
}

/**
 * Callback function to display the applet
 */
function _freemind_display_applet(){
  return '<applet code="freemind.main.FreeMindApplet.class" archive=
  "'.base_path().drupal_get_path('module', 'freemind').'/freemindbrowser.jar" width="100%" height="500px">
    <param name="type" value=
    "application/x-java-applet;version=1.4" />
    <param name="scriptable" value="true" />
    <param name="modes" value=
    "freemind.modes.browsemode.BrowseMode" />
    <param name="browsemode_initial_map" value=
    "http://'.$_SERVER['HTTP_HOST'].url('admin/content/taxonomy/' . arg(3) . '/mm').'/freemind.mm" />
    <param name="initial_mode" value="Browse" />
    <param name="selection_method" value=
    "selection_method_direct" />
  </applet>';
}

/**
 * Callback function to display the mm
 */
function _freemind_display_mm(){
  $vid = arg(3);
  $v = taxonomy_vocabulary_load($vid);
  if(arg(5) === null){
    header('Content-type: application/mm+xml');
    header('Content-Disposition: attachment; filename="'.$v->name.'.mm"');
  }
  echo(_freemind_taxexport_render_mm($vid));
  exit();
}
