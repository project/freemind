<?php

/**
  * Implementation of hook_views_plugin().
  */
function freemind_views_plugins() {
  return array(
    'display' => array(
      'freemind' => array(
        'title' => t('Freemind'),
        'help' => t('Display the view as a Freemind mm file.'),
        'handler' => 'views_plugin_display_freemind',
        'parent' => 'page', // so it knows to load the page plugin .inc file
        'uses hook menu' => TRUE,
        'use ajax' => FALSE,
        'use pager' => FALSE,
        'accept attachments' => FALSE,
        'admin' => t('Freemind'),
      ),
    ),
    'style' => array(
      'mm' => array(
        'title' => t('MM file'),
        'help' => t('Generates an MM file from a view.'),
        'handler' => 'views_plugin_style_mm',
        'theme' => 'views_view_mm',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'freemind',
      ),
    ),
  );
}