<?php

class views_plugin_display_freemind extends views_plugin_display_page{
    function uses_breadcrumb() { return FALSE; }
    function get_style_type() { return 'freemind'; }

    function option_definition() {
        $options = parent::option_definition();

        $options['style_plugin']['default'] = 'mm';

        return $options;
    }

    function options_summary(&$categories, &$options){
      parent::options_summary($categories, $options);
      $categories['page'] = array('title'=>t('Freemind settings'));
      #static $d = false; if(!$d){ var_dump($categories); var_dump($options); $d = true; }
    }

    function options_form(&$form, &$form_state){
      parent::options_form($form, $form_state);
      
    }
}